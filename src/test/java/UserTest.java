import constants.StatusCode;
import entites.User;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.restassured.response.Response;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import requests.user.UserRequest;
import responses.CreateUserResponse;
import responses.GetUserResponse;
import responses.UpdateUserResponse;
import utils.JsonHelper;

@Feature("Verify CRUD Operations on User module")
public class UserTest {

    JSONObject testdata;

    @Description("Get the test data")
    @BeforeClass
    public void setupData() {
        JsonHelper jsonHelper = new JsonHelper();
        testdata = jsonHelper.getJsonObject("testdata.json");
    }

    @Story("POST request")
    @Description("Verify user creation functionality")
    @Test
    public void shouldBeAbleToCreateUser() {
        User user = (User) JsonHelper.getObject(String.valueOf(testdata.get("user1")), User.class);

        CreateUserResponse createUserResponse = new UserRequest().create(user);

        Assert.assertEquals(createUserResponse.getStatusCode(), StatusCode.CODE_201.getCode());
        Assert.assertNotNull(createUserResponse.getId(), "user id is null/empty");
        Assert.assertEquals(createUserResponse.getName(), user.getName(), "User name is not as expected");
        Assert.assertEquals(createUserResponse.getJob(), user.getJob(), "User job is not as expected");
    }

    @Story("PUT request")
    @Description("Verify user name and job is updated")
    @Test
    public void shouldBeAbleToUpdateUser() {
        User user = (User) JsonHelper.getObject(String.valueOf(testdata.get("user1")), User.class);
        UserRequest userRequest = new UserRequest();
        CreateUserResponse response = userRequest.create(user);
        Assert.assertEquals(response.getStatusCode(), StatusCode.CODE_201.getCode());
        user = (User) JsonHelper.getObject(String.valueOf(testdata.get("updateUserDetails")), User.class);

        UpdateUserResponse userUpdateResponse = userRequest.update(response.getId(), user);

        Assert.assertEquals(userUpdateResponse.getStatusCode(), StatusCode.CODE_200.getCode());
        Assert.assertEquals(userUpdateResponse.getName(), user.getName(), "User name is not as expected");
        Assert.assertEquals(userUpdateResponse.getJob(), user.getJob(), "User job is not as expected");
    }

    @Story("GET request")
    @Description("Verify the user details of user id 2")
    @Test
    public void shouldBeAbleGetUserById() {
        JSONObject user2Details = testdata.getJSONObject("user2");

        GetUserResponse getUserResponse = new UserRequest().get(user2Details.getInt("id"));

        Assert.assertEquals(getUserResponse.getStatusCode(), StatusCode.CODE_200.getCode());
        Assert.assertEquals(getUserResponse.getData().getEmail(), user2Details.get("email"), "Email id is not as expected");
        Assert.assertEquals(getUserResponse.getData().getFirst_name(), user2Details.get("firstName"), "First name is not as expected");
        Assert.assertEquals(getUserResponse.getData().getLast_name(), user2Details.get("lastName"), "Last name is not as expected");
    }

    @Story("DELETE request")
    @Description("Verify user with given id is deleted")
    @Test
    public void shouldBeAbleToDeleteUser() {
        User user = (User) JsonHelper.getObject(String.valueOf(testdata.get("user1")), User.class);
        UserRequest userRequest = new UserRequest();
        CreateUserResponse response = userRequest.create(user);
        Assert.assertEquals(response.getStatusCode(), StatusCode.CODE_201.getCode());

        Response deleteUserResponse = userRequest.delete(response.getId());

        Assert.assertEquals(deleteUserResponse.getStatusCode(), StatusCode.CODE_204.getCode());
    }
}
